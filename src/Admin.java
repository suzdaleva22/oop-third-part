/**
 * Создание финализированного класса с использованием родительского класса
 * Override метода
 */
public class Admin extends Person{
    Admin(String name, int age) {
        super(name, age);
        System.out.println("Constructor Admin name: " + name + " age: " + age);
    }

    @Override
    void changePassword() {
        System.out.println("Admin does not have the right to change the password");
    }
}
