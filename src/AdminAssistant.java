/**
 * Создание финализированного класса с использованием родительского класса
 * Override метода
 */
public final class AdminAssistant extends Admin {
    AdminAssistant(String name, int age) {
        super(name, age);
        System.out.println("Final constructor Admin Assistant name: " + name + " age: " + age);
    }
    @Override
    void changePassword() {
        System.out.println("Admin Assistant does not have the right to change the password");
    }
}
