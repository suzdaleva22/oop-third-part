
public class Main {
    public static void main(String[] args) {
/////////// part 1
//        Person admin = new Person("Alina", 22);
//        admin.setAge(30);
//        System.out.println("PublicClass: Name = " + admin.getName() + ", Age = " + admin.getAge());
//
//        Person superadmin = new Person("Alex", 38);
//        System.out.println("PublicClass: Name = " + superadmin.getName() + ", Age = " + superadmin.getAge());
//        System.out.println("Number of Users: " + Person.getNumberOfUsers());

//        Person.Nested nested = admin.new Nested();
//        nested.print();
//
//        Person.NestedStatic nestedStatic = new Person.NestedStatic();
//        nestedStatic.print();
//
//        admin.localClass();
//        superadmin.performAction();

        //////////part 2

        Person admin2 = new Admin("Jim", 40);
        Person superadmin2 = new Superadmin("Sem");
        Person superadmin3 = new Superadmin("Sem", "full");
        Admin adminAssistant = new AdminAssistant("Georg", 65);
        System.out.println(Person.numberOfPersons);

        admin2.changePassword();
        superadmin2.changePassword();
        adminAssistant.changePassword();

        ////////////part 3
        User.search();
        System.out.println(Language.UA);
        System.out.println(Language.UA.getName());
    }
}