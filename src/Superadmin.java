/**
 * Создание финализированного класса с использованием родительского класса
 * Override метода
 * Overload
 * Приватная переменная с get - set
 */
public class Superadmin extends Person{
    private String access;

    public Superadmin(String name) {
        super(name);
        System.out.println("Constructor Superadmin name: "+ name);
    }
    public Superadmin(String name, String access) {
        super(name);
        this.access = access;
        System.out.println("Constructor Superadmin2 name: "+ name +"  access: " + access);
    }
    public String getAccess() {
        return access;
    }
    public void setAccess(String access) {
        this.access = access;
    }
    @Override
    void changePassword() {
        System.out.println("Superadmin has the right to change the password");
    }
}
