/**
 * Перечень языков
 */
public enum Language {
    UA("ukrainian"),
    EN("english"),
    OTHER("other");

    private String name;
    Language (String name) {
         this.name = name;
    }

    public String getName() {
        return name;
    }
}
